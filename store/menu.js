import { defineStore } from 'pinia'
export const useMenuStore = defineStore('menu', () => {

    const language = ref({ code: 'ru' })

    const sloganFirst = ref({
        ru: 'Ресторан предлагает оригинальное меню в стиле фьюжн',
        me: 'Restoran nudi originalni fjužn meni',
        en: 'The restaurant offers an original fusion menu',
    })

    const categories = ref({
        menu: {
            en: 'Menu',
            ru: 'Меню',
            me: 'Meni'
        },
        home: {
            en: 'Home',
            ru: 'Главная',
            me: 'Početak'
        },
        contacts: {
            en: 'Contacts',
            ru: 'Контакты',
            me: 'Kontakti'
        }
    })

    const sloganSecond = ref({
        ru: 'сочетающее в себе популярные блюда европейской и азиатской кухни',
        me: 'kombinujući popularna jela evropske i azijske kuhinje',
        en: 'combining popular dishes of European and Asian cuisine',
    })

    const booking = ref({
        ru: 'Бронирование столов и доставка',
        me: 'Rezervacije stolova i dostava',
        en: 'Booking and delivery',
    })

    const menu = reactive({
        appetizers: {
            title: {
                en: 'Appetizers',
                ru: 'Закуски',
                me: 'Predjela'
            },
            items: [
                {
                    title: {
                        en: 'Tzatziki',
                        ru: 'Цацике',
                        me: 'Tzatzike'
                    },
                    description: {
                        en: 'Cold appetizer sauce made from yogurt, fresh cucumber and garlic, a traditional dish of Greek cuisine.',
                        ru: 'Холодный соус-закуска из йогурта, свежего огурца и чеснока, традиционное блюдо греческой кухни.',
                        me: 'Hladno predjelo sos od jogurta, svežeg krastavca i belog luka, tradicionalno jelo grčke kuhinje.'
                    },
                    img: '/img/menu/appetizers/tzatziki.webp',
                    weight: 100,
                    price: 2.5
                },
                {
                    title: {
                        en: 'Hummus',
                        ru: 'Хумус',
                        me: 'Humus'
                    },
                    description: {
                      en: 'In Arabic, the word hummus refers to both the chickpea plant (Cicer arietinum) and the snack itself. Widely distributed in the countries of the Middle East, in modern times it has become famous throughout the world. Used as an independent dish, as a sauce, dressing for other dishes, as a spread.',
                      ru: 'В арабском языке слово «хумус» означает как растение нут бараний (Cicer arietinum), так и саму закуску. Широко распространён в странах Ближнего Востока, в современности получил известность во всём мире. Используется как самостоятельное блюдо, как соус, заправка для других блюд, как спред.',
                      me: 'Na arapskom, reč humus se odnosi i na biljku slanutka (Cicer arietinum) i na samu užinu. Široko rasprostranjen u zemljama Bliskog istoka, u moderno doba postao je poznat širom sveta. Koristi se kao samostalno jelo, kao sos, preliv za druga jela, kao namaz.'
                    },
                    img: '/img/menu/appetizers/hummus.webp',
                    weight: 100,
                    price: 2.5,
                    vegan: true
                },
                {
                    title: {
                        en: 'Labneh',
                        ru: 'Лабне',
                        me: 'Labneh'
                    },
                    description: {
                        en: 'Labneh is a traditional Middle Eastern cream cheese made from yogurt. The name “Labneh” means “milk” or “white” in Arabic. It belongs to young cheeses',
                        ru: 'Лабне – это традиционный крем-сыр кухни стран Ближнего Востока, приготовленный из йогурта. Название «Лабне» с арабского означает «молоко» или «белый». Он относится к молодым сырам',
                        me: 'Labneh je tradicionalni bliskoistočni krem sir napravljen od jogurta. Ime „Labneh“ na arapskom znači „mleko“ ili „belo“. Spada u mlade sireve'
                    },
                    img: '/img/menu/appetizers/labneh.webp',
                    weight: 100,
                    price: 2.5
                },
                {
                    title: {
                        en: 'Red bell pepper dip',
                        ru: 'Дип из печеных перцев',
                        me: 'Dip od pečene paprike'
                    },
                    description: {
                        en: 'This is a dish that has its roots in the traditional cuisine of various countries. It is usually served as a sauce or addition to other dishes. Historically, roasted peppers have been widely used in cooking across cultures. The technique of roasting or roasting peppers helps impart a unique aroma and flavor, as well as making them softer and easier to grind. This dish is an excellent example of the combination of traditional cooking methods with modern culinary techniques. It allows you to enjoy the rich taste and aroma of roasted peppers and add a touch of exoticism to your culinary collection.',
                        ru: 'Это блюдо, которое имеет свои корни в традиционной кухне различных стран. Он обычно подается в качестве соуса или добавки к другим блюдам. Исторически, печеные перцы были широко использованы в кулинарии разных культур. Техника обжаривания или запекания перцев помогает придать им уникальный аромат и вкус, а также делает их более мягкими и легко перетираемыми. Это блюдо является прекрасным представителем сочетания традиционных методов приготовления современной кулинарной техники. Оно позволяет насладиться богатым вкусом и ароматом печеных перцев и добавить нотки экзотики в вашу кулинарную коллекцию.',
                        me: 'Ovo je jelo koje vuče korene iz tradicionalne kuhinje raznih zemalja. Obično se služi kao sos ili dodatak drugim jelima. Istorijski gledano, pečene paprike su se široko koristile u kuvanju u različitim kulturama. Tehnika pečenja ili pečenja paprike daje jedinstvenu aromu i ukus, kao i da je mekša i lakša za mlevenje. Ovo jelo je odličan primer kombinacije tradicionalnih metoda kuvanja sa savremenim kulinarskim tehnikama. Omogućava vam da uživate u bogatom ukusu i aromi pečenih paprika i dodate dašak egzotike vašoj kulinarskoj kolekciji.'
                    },
                    img: '/img/menu/appetizers/dip_pepper.webp',
                    weight: 100,
                    price: 2.5,
                    hot: true,
                    vegan: true
                },
                {
                    title: {
                        en: 'Baba Ghanoush',
                        ru: 'Бабагануш',
                        me: 'Babaganuš'
                    },
                    description: {
                        en: 'An oriental dish, an appetizer consisting primarily of pureed cooked eggplant mixed with Middle Eastern seasonings.',
                        ru: 'Блюдо восточной кухни, закуска, состоящая главным образом из пюрированных готовых баклажанов, смешанных с ближне-восточными приправами.',
                        me: 'Orijentalno jelo, predjelo koje se sastoji prvenstveno od pasiranog kuvanog patlidžana pomešanog sa bliskoistočnim začinima.'
                    },
                    img: '/img/menu/appetizers/babaganush.webp',
                    weight: 100,
                    price: 2.5,
                    vegan: true
                },
                {
                    title: {
                        en: 'Ezme',
                        ru: 'Эзме',
                        me: 'Ezme'
                    },
                    description: {
                      en: 'Ajili ezme is a vegetable appetizer in Turkish cuisine, a type of meze. We make it based on baked and fresh tomatoes with sweet peppers with the addition of walnuts and herbs.',
                      ru: 'Аджилы́ эзме́ - овощная закуска в турецкой кухне, разновидность мезе. Делаем мы его на основе печных и свежих томатов со сладким перцем с добавление грецкого ореха и зелени.',
                      me: 'Ajili ezme je predjelo od povrća u turskoj kuhinji, vrsta meze. Pravimo ga na bazi pečenog i svežeg paradajza sa slatkom paprikom uz dodatak oraha i začinskog bilja.'
                    },
                    img: '/img/menu/appetizers/ezme.webp',
                    weight: 100,
                    price: 2.5,
                    vegan: true
                },
                {
                    title: {
                        en: 'Mezze - Middle Eastern dip platter',
                        ru: 'Мезе - Набор ближневосточных закусок',
                        me: 'Meze - set sredneistočnih predjela'
                    },
                    description: {
                        en: 'The set features a variety of Middle Eastern appetizers, served with pitas that we bake ourselves.',
                        ru: 'Набор представляет из себя множество ближне восточных закусок, подаётся с питами, которые мы сами испекаем.',
                        me: 'Set se sastoji od raznih bliskoistočnih predjela, serviranih sa pitama koje sami pečemo.'
                    },
                    img: '/img/menu/appetizers/meze_small.webp',
                    weight: '6 * 50',
                    price: 6.5
                },
                {
                    title: {
                        en: 'Mezze - Middle Eastern dip platter for 2-3',
                        ru: 'Мезе - Набор ближневосточных закусок на 2-3 персоны',
                        me: 'Meze - set sredneistočnih predjela za 2-3 osobe'
                    },
                    description: {
                        en: 'The set features a variety of Middle Eastern appetizers, served with pitas that we bake ourselves.',
                        ru: 'Набор представляет из себя множество ближне восточных закусок, подаётся с питами, которые мы сами испекаем.',
                        me: 'Set se sastoji od raznih bliskoistočnih predjela, serviranih sa pitama koje sami pečemo.'
                    },
                    img: '/img/menu/appetizers/meze_big.webp',
                    weight: '6 * 100',
                    price: 13
                },
                {
                    title: {
                        en: 'Falafel platter',
                        ru: 'Фалафель',
                        me: 'Falafel'
                    },
                    description: {
                        en: 'A dish consisting of deep-fried balls of chopped chickpeas (or beans), served with pita, a salad of two types of cabbage',
                        ru: 'Фала’фель (фаляфель) — блюдо, представляющее собой жаренные во фритюре шарики из измельченного нута (или бобов), подаётся с питой, салатом из двух видов капусты',
                        me: 'Jelo koje se sastoji od prženih kuglica seckanog slanutka (ili pasulja), servirano sa pita, salatom od dve vrste kupusa',
                    },
                    img: '/img/menu/appetizers/falafel.webp',
                    weight: 200,
                    price: 6,
                    vegan: true
                },
                {
                    title: {
                        en: 'Bruskhetta with anchovies',
                        ru: 'Брускетта с томатами и анчоусами',
                        me: 'Bruskete sa paradajzom i inčunima'
                    },
                    description: {
                        en: 'A truly Italian warm appetizer with fresh tomatoes with a light taste of balsam and anchovy.',
                        ru: 'Исконно итальянская тёплая закуска со свежими помидорами с легким вкусом бальзамина редакшн и анчоуса.',
                        me: 'Zaista italijansko toplo predjelo sa svežim paradajzom sa blagim ukusom balzama i inćuna.'
                    },
                    img: '/img/menu/appetizers/brusketta_anchoise.webp',
                    weight: 200,
                    price: 4.5
                },
                {
                    title: {
                        en: 'Tuna tartare',
                        ru: 'Тартар из тунца',
                        me: 'Tartar od tune'
                    },
                    checkAvailability: {
                        en: 'Sezonsko jelo! Pitajte za dostupnost!',
                        ru: 'Сезонное блюдо! Спрашивайте о наличии.',
                        me: 'Seasonal dish! Ask for availability.',
                    },
                    description: {
                        en: 'Appetizer of fresh tuna with the addition of avocado, tomatoes and pickled cucumbers with capers, seasoned with a special Thai sauce with a slight piquancy.',
                        ru: 'Закуска из свежего тунца с добавлением авокадо, помидор и маринованных огурцов с каперсами, заправленный особым тайским соусом с легкой пикантностью.',
                        me: 'Predjelo od sveže tunjevine sa dodatkom avokada, paradajza i kiselih krastavaca sa kaparima, začinjeno posebnim tajlandskim sosom sa blagom pikantnošću.'
                    },
                    img: '/img/menu/appetizers/tartar.webp',
                    weight: 200,
                    price: 8.5
                },
                {
                    title: {
                        en: 'Spring roll',
                        ru: 'Спринг рол',
                        me: 'Spring rol'
                    },
                    description: {
                        en: 'Spring rolls are rice paper rolls with fresh vegetables and tiger prawns wrapped in them. Served fried with a golden crust.',
                        ru: 'Спринг-роллы представляют собой рулетики из рисовой бумаги, в которую завернуты свежие овощи и тигровые креветки. Подаются обжаренные с золотистой корочкой.',
                        me: 'Prolećne rolnice su rolnice od pirinčanog papira sa svežim povrćem i tigrastim kozicama umotanim u njih. Služi se prženo sa zlatnom korom.'
                    },
                    img: '/img/menu/appetizers/spring_roll.webp',
                    weight: 200,
                    price: 6.5
                },
                {
                    title: {
                        en: 'Shrimps',
                        ru: 'Креветки',
                        me: 'Škampi'
                    },
                    description: {
                      en: 'Breaded fried shrimp. For this dish we use Argentinean Langoustine shrimp with a delicate sweetish taste. Served with lime zest grated on top.',
                      ru: 'Жареные креветки в панировке. Для этого блюда мы используем аргентинскую креветку «Лангустин» с нежным сладковатым вкусом. Подаётся с натертой сверху цедрой лайма.',
                      me: 'Pohovane pržene škampe. Za ovo jelo koristimo argentinske škampe langoustine delikatnog slatkastog ukusa. Služi se sa naribanom koricom od limete.',
                    },
                    img: '/img/menu/appetizers/shrimps.webp',
                    weight: 200,
                    price: 9.5
                },

            ]
        },
        salads: {
            title: {
                en: 'Salads',
                ru: 'Салаты',
                me: 'Salate'
            },
            items: [
                {
                    title: {
                        en: 'Greek salad',
                        ru: 'Греческий салат',
                        me: 'Grčka salata'
                    },
                    description: {
                        en: 'Traditional recipe for a wonderful Greek salad with the addition of two varieties of dried olives.',
                        ru: 'Традиционный рецепт прекрасного греческого салата с добавлением двух сортов вяленых олив.',
                        me: 'Tradicionalni recept za divnu grčku salatu sa dodatkom dve vrste suvih maslina.'
                    },
                    img: '/img/menu/salads/greek_salad.webp',
                    weight: 300,
                    price: 5.5
                },
                {
                    title: {
                        en: 'Cucumber salad with Chinese sauce',
                        ru: 'Салат из огурцов',
                        me: 'Salata od krastavca, kineski sos'
                    },
                    description: {
                      ru: 'Хрустящие парниковые огурцы маринованные в нашем соусе с добавлением бальзамик редакш, имбирём, мелко нарезанным Чили и кунжутном масле. Это блюдо хороший «аперитив» перед началом вашей трапезы.',
                      en: 'Crispy greenhouse cucumbers marinated in our sauce with the addition of balsamic reduction, ginger, finely chopped chili and sesame oil. This dish is a good “aperitif” before starting your meal.',
                      me: 'Hrskavi staklenički krastavci marinirani u našem sosu sa dodatkom balzamiko redukcije, đumbira, sitno seckanog čilija i susamovog ulja. Ovo jelo je dobar "aperitiv" pre početka obroka.',
                    },
                    img: '/img/menu/salads/cucumber_and_arugula_salad_with_chinese_sauce.webp',
                    weight: 250,
                    price: 4.5,
                    vegan: true
                },
            ]
        },
        soups: {
            title: {
                en: 'Soups',
                ru: 'Супы',
                me: 'Supe'
            },
            items: [
                {
                    title: {
                        en: 'Pumpkin cream soup',
                        ru: 'Крем-суп из тыквы',
                        me: 'Kremasta supa od bundeve'
                    },
                    description: {
                        en: 'The soup is native to North and South Africa. Made with pumpkin and sweet potato, cream and freshly grated nutmeg.',
                        ru: 'Суп родом  из Северной и Южной Африки. Готовится из Тыквы и сладкого батата с добавлением сливок и свеже натертого мускатного ореха.',
                        me: 'Supa je poreklom iz Severne i Južne Afrike. Napravljen od bundeve i slatkog krompira, kajmaka i sveže rendanog muškatnog oraščića.'
                    },
                    img: '/img/menu/soups/pumpkin_cream_soup.webp',
                    weight: 200,
                    price: 4.5,
                    vegan: true
                },
                {
                    title: {
                        en: 'Red bell pepper soup with goat cheese',
                        ru: 'Суп из печеных перцев с козьим сыром',
                        me: 'Supa od pečene paprike ssa kozjim sirom'
                    },
                    description: {
                        en: 'Roasted peppers with a hint of roasted tomato and red onion. A savory soup with the addition of goat cheese quenelle to balance out the heat.',
                        ru: 'Печеные перцы с легким оттенком печеного помидора и красного лука. Пикантный суп с добавлением кнель из козьего сыра, который балансирует остроту.',
                        me: 'Pečene paprike sa primesama pečenog paradajza i crvenog luka. Slana supa sa dodatkom kenela od kozjeg sira da uravnoteži toplotu.'
                    },
                    img: '/img/menu/soups/red_bell_pepper_soup_with_goat_cheese.webp',
                    weight: 200,
                    price: 4.5,
                    hot: true,
                    vegan: true
                },
                {
                    title: {
                        en: 'Vietnamese Pho soup with beef',
                        ru: 'Суп Фо Бо из говядины вьетнамский',
                        me: 'Vijetnamska teleča supa Fo Bo'
                    },
                    description: {
                        en: 'Strong broth with the addition of Thai spices. The broth is served with rice noodles, thinly sliced beef, red onion and freshly chopped herbs.',
                        ru: 'Крепкий бульон с добавлением тайских специй. Подается бульон с рисовой лапшой, тонко нарезанной говядиной, красным луком и свеже нарезанной зеленью.',
                        me: 'Jaka čorba sa dodatkom tajlandskih začina. Čorba se služi sa pirinčanim rezancima, tanko isečenom govedinom, crvenim lukom i sveže seckanim začinskim biljem.'
                    },
                    img: '/img/menu/soups/vietnamese_pho_with_beaf.webp',
                    weight: 700,
                    price: 9.5
                },
                {
                    title: {
                        en: 'Chicken noodle soup',
                        ru: 'Суп куриный с лапшой',
                        me: 'Pileća supa sa rezancima'
                    },
                    description: {
                      ru: 'Классический чистый куриный бульон с морковью, цукини и куриным кнелями. Прекрасный и самое гипоаллергенный суп для вашего ребекка.',
                      en: 'Classic clear chicken broth with carrots, zucchini and chicken quenelles. A wonderful and most hypoallergenic soup for your Rebecca.',
                      me: 'Klasična bistra pileći bujon sa šargarepom, tikvicama i pilećim kenelima. Divna i najhipoalergenija supa za vašu Rebeku.'
                    },
                    img: '/img/menu/kids_menu/chicken_noodle_soup.webp',
                    weight: 300,
                    price: 5
                },
            ]
        },
        pasta_and_risotto: {
            title: {
                en: 'Pasta and risotto',
                ru: 'Паста и ризотто',
                me: 'Paste i rižote'
            },
            items: [
                {
                    title: {
                        en: 'Linguini with meatballs and tomato sauce',
                        ru: 'Лингвини с томатом и фрикадельками',
                        me: 'Linguini sa paradajzom i ćufte'
                    },
                    description: {
                        ru: 'Классическая паста с томатным соусом и кнелями из говядины с зеленью. Подаётся с натертым сыром Пармезан.',
                        en: 'Classic pasta with tomato sauce and beef quenelles with herbs. Served with grated Parmesan cheese.',
                        me: 'Klasična pasta sa paradajz sosom i goveđim kenelima sa začinskim biljem. Služi se sa rendanim parmezanom.',
                    },
                    img: '/img/menu/kids_menu/linguini_with_meatballs_and_tomato_sauce.webp',
                    weight: 260,
                    price: 4.5
                },
                {
                    title: {
                        en: 'Linguini arabiata',
                        ru: 'Лингвини арабьята',
                        me: 'Linguini arabiata'
                    },
                    description: {
                        en: 'Classic pasta with spicy tomato sauce and garlic. Arabiata means "angry" in Italian, a reference to the pasta being spicy.',
                        ru: 'Классическая паста с пикантным томатным соусом и чесноком. Арабьята в переводе с итальянского означает «сердитый», намек на то, что паста острая.',
                        me: 'Klasična pasta sa pikantnim paradajz sosom i belim lukom. Arabiata na italijanskom znači "ljuta", što znači da je testenina začinjena.'
                    },
                    img: '/img/menu/pasta_and_risotto/linguini_arabiata.webp',
                    weight: 300,
                    price: 6.5,
                    hot: true,
                    vegan: true
                },
                {
                    title: {
                        en: 'Carbonara',
                        ru: 'Карбонара',
                        me: 'Karbonara'
                    },
                    description: {
                        'ru': 'Спагетти с мелкими кусочками панчетты, смешанная с соусом из яиц, сыра пекорино романо и грано подано, и свежемолотого чёрного перца. Не избитая классика Итальянской кухни.',
                        'en': 'Spaghetti with small pieces of pancetta, mixed with a sauce of eggs, pecorino Romano and grano cheese, and freshly ground black pepper. Not a hackneyed classic of Italian cuisine.',
                        'me': 'Špagete sa sitnim komadićima pancete, pomešane sa sosom od jaja, pecorino romano i grano sira, i sveže mlevenog crnog bibera. Nije izoštreni klasik italijanske kuhinje.'
                    },
                    img: '/img/menu/pasta_and_risotto/carbonara.webp',
                    weight: 250,
                    price: 9.5
                },
                {
                    title: {
                        en: 'Saffron risotto with prawns',
                        ru: 'Ризотто с креветками и шафраном',
                        me: 'Rižoto sa škampima i šafranom'
                    },
                    description: {
                        en: 'Classic creamy chevron risotto with the addition of 5 tiger shrimps, sprinkled with shrimp oil.',
                        ru: 'Классическое сливочно-шаврановое ризотто с добавлением 5-ти тигровых креветок, политое креветочным маслом.',
                        me: 'Klasični kremasti ševron rižoto sa dodatkom 5 tigrastih škampa, poprskanih uljem od škampa.'
                    },
                    img: '/img/menu/pasta_and_risotto/safron_risotto_with_prawns.webp',
                    weight: 200,
                    price: 9.5
                },
                {
                    title: {
                        en: 'Risotto with truffle',
                        ru: 'Ризотто с трюфелем',
                        me: 'Rižoto sa tartufima'
                    },
                    description: {
                      en: 'Classic risotto with the addition of truffle and porcini mushroom. Creamy mushroom flavor with perfectly ripened grano cheese served with a slight truffle aftertaste.',
                      ru: 'Классическое ризотто с добавлением трюфеля и белого гриба. Сливочно грибной вкус с терпим созревшим сыром грано подано имеющее легкое трюфельное послевкусие.',
                      me: 'Klasični rižoto sa dodatkom tartufa i vrganja. Kremast ukus pečuraka sa podnošljivo zrelim grano sirom serviranim sa blagim ukusom tartufa.'
                    },
                    img: '/img/menu/pasta_and_risotto/truffel.webp',
                    weight: 200,
                    price: 9.5
                }
            ]
        },
        sandwiches: {
            title: {
                en: 'Sandwiches',
                ru: 'Сэндвичи',
                me: 'Sendviči'
            },
            items: [
                {
                    title: {
                        en: 'Chicken and vegetable sandwich',
                        ru: 'Сендвич с курицей и овощами',
                        me: 'Sendvič sa piletinom i povrčem'
                    },
                    img: '/img/menu/sandwiches/chicken_and_vegetables_sandwich.webp',
                    weight: 270,
                    price: 5.5
                },
                {
                    title: {
                        en: 'Falafel sandwich',
                        ru: 'Сендвич с фалафелем',
                        me: 'Falafel sendvič'
                    },
                    description: {
                        en: 'Sandwich with fresh vegetables and cabbage with the addition of falafel hemispheres with sesame sauce. The sandwich is served in pita bread, which we bake ourselves.',
                        ru: 'Сендвич со свеже и овощами и капустой с добавлением полусфер из фалафеля с кунжутным соусом. Сендвич подаётся в пите, которую мы испекаем лично.',
                        me: 'Sendvič sa svežim povrćem i kupusom sa dodatkom falafel hemisfera sa susamom sosom. Sendvič se servira u pita hlebu, koji sami pečemo.'
                    },
                    img: '/img/menu/sandwiches/falafel_sandwich.webp',
                    weight: 300,
                    price: 5.5
                },
                {
                    title: {
                        en: 'Croissant with salmon',
                        ru: 'Круассан с лососем',
                        me: 'Kroasan sa lososom'
                    },
                    description: {
                      en: 'A versatile dish for breakfast or lunch with coffee. Fresh croissant with salted salmon, fresh tomato, avocado and cream cheese.',
                      ru: 'Универсальное блюдо для завтрака или обеда с кофе. Свежий круассан с засоленным лососем, свежим помидором, авокадо и творожным сыром.',
                      me: 'Svestrano jelo za doručak ili ručak uz kafu. Sveži kroasan sa slanim lososom, svežim paradajzom, avokadom i krem sirom.',
                    },
                    img: '/img/menu/sandwiches/croissant_with_salmon.webp',
                    weight: 200,
                    price: 6.5
                },{
                    title: {
                        en: 'Sandwich with ham and cheese',
                        ru: 'Сэндвич с ветчиной и сыром',
                        me: 'Sendvič sa šunkom i sirom'
                    },
                    description: {
                      en: 'Classic English sandwich with Gouda cheese and Ham, grilled.',
                      ru: 'Классический английский сендвич с сыром Гауда и Ветчиной, обжаренный на гриле.',
                      me: 'Klasični engleski sendvič sa gauda sirom i šunkom, na žaru.'
                    },
                    img: '/img/menu/sandwiches/ham_and_cheese.webp',
                    weight: 100,
                    price: 2.5
                },
                {
                    title: {
                        en: 'Sous vide chicken sandwich with pancetta',
                        ru: 'Сендвич с курицей су-вид и панчетой',
                        me: 'Sous vide pileći sendvič sa pancetom'
                    },
                    description: {
                        en: 'Sandwich with tender chicken cooked sous vide to preserve its nutritional properties and fresh vegetables. The sandwich is served in pita bread, which we bake ourselves.',
                        ru: 'Сендвич с нежной курицей приготовленной способом су-вид для сохранения её питательных свойств и свежеми овощами. Сендвич подаётся в пите, которую мы испекаем лично.',
                        me: 'Sendvič sa mekom piletinom kuvanom sous vide da bi se sačuvala njegova hranljiva svojstva i sveže povrće. Sendvič se servira u pita hlebu, koji sami pečemo.'
                    },
                    img: '/img/menu/sandwiches/pancheta.webp',
                    weight: 270,
                    price: 5.5
                },
            ]
        },
        main_course: {
            title: {
                en: 'Main course',
                ru: 'Основные блюда',
                me: 'Glavna jela'
            },
            items: [
                {
                    title: {
                        en: 'Chicken fried rice',
                        ru: 'Курица с рисом по-китайски',
                        me: 'Piletina sa pirinčem u kineskom sosu'
                    },
                    description: {
                        en: 'Chicken fried with vegetables and rice in a wok with the addition of scraper and signature Thai sauce.',
                        ru: 'Курица обжаренная с овощами и рисом в воке с добавлением скребла и фирменного тайского соуса.',
                        me: 'Piletina pržena sa povrćem i pirinčem u voku sa dodatkom strugača i prepoznatljivog tajlandskog sosa.'
                    },
                    img: '/img/menu/main_course/chicken_fried_rice.webp',
                    weight: 300,
                    price: 7
                },
                {
                    title: {
                        en: 'Poke (rice and vegetables) with tuna or salmon',
                        ru: 'Поке (рис и овощи) с тунцом или лососем',
                        me: 'Poke (pirinač i povrče) sa tunom ili l   sosom'
                    },
                    checkAvailability: {
                        ru: 'Узнавайте по наличию варианта с тунцом.',
                        en: 'Find out by availability with tuna.',
                        me: 'Сазнајте по доступности са туњевином.',
                    },
                    description: {
                        en: 'One of the main dishes of Hawaiian cuisine, salad made from raw diced fish. In this case, we serve it with seasoned rice and fresh vegetables with nori leaves.',
                        ru: 'Одно из основных блюд гавайской кухни, салат из сырой рыбы, нарезанной кубиками. В данном случаем мы подаём его с заправленным рисом и свежими овощами с листьями нори.',
                        me: 'Jedno od glavnih jela havajske kuhinje, salata od sirove ribe isečene na kockice. U ovom slučaju služimo ga sa začinjenim pirinčem i svežim povrćem sa listovima nori.'
                    },
                    img: '/img/menu/main_course/poke_tuna_or_salmon.webp',
                    weight: 300,
                    price: 9.5
                },
                {
                    title: {
                        en: 'Pork tenderloins on pineapple with chimichurri sause',
                        ru: 'Медальоны из свинины на ананасе, соус чимичурри',
                        me: 'Svinjski medaljoni na ananasu, čimičuri sos'
                    },
                    description: {
                        en: 'Grilled pork medallions with pancetta and pineapple. Served with fresh chimichurri sauce.',
                        ru: 'Свиные медальоны обжаренные на гриле с панчетой и ананасом. Подается с свежим соусом чимичури.',
                        me: 'Svinjski medaljoni na žaru sa pancetom i ananasom. Služi se sa svežim ćimičuri sosom.'
                    },
                    img: '/img/menu/main_course/chimichurri.webp',
                    weight: 420,
                    price: 11
                },
                {
                    title: {
                        en: 'Seared sesame tuna with Thai style salad',
                        ru: 'Тунец в кунжуте с тайским салатом',
                        me: 'Tuna u susanu sa tajlandskim salatom'
                    },
                    description: {
                        en: 'Seared tuna in sesame, blue done. Served with fresh mango, cucumber and carrot salad topped with signature Thai sauce.',
                        ru: 'Опаленный тунец в кунжуте  имеющий прожарку blue. Подается со свежем салатом из манго огурца и моркови политым фирменным тайским соусом.',
                        me: 'Pečena tunjevina u susamu, plava gotova. Služi se sa svežom salatom od manga, krastavca i šargarepe prelivenom prepoznatljivim tajlandskim sosom.'
                    },
                    img: '/img/menu/main_course/tuna.webp',
                    weight: 300,
                    price: 15,
                    checkAvailability: {
                        en: 'Sezonsko jelo! Pitajte za dostupnost!',
                        ru: 'Сезонное блюдо! Спрашивайте о наличии.',
                        me: 'Seasonal dish! Ask for availability.',
                    }
                },
                {
                    title: {
                        en: 'Beef tenderloin fajitas',
                        ru: 'Фахитос из говядины (вырезка)',
                        me: 'Goveđi fajitas (filet)'
                    },
                    description: {
                        en: 'Fajitas are one of the most famous Mexican dishes, which have become popular far beyond Latin America. Served with roasted vegetables, it comes with pico de gayo, avocado sauce and Greek yogurt.',
                        ru: 'Фахитос — одно из известнейших мексиканских блюд, ставшее популярным далеко за пределами Латинской Америки. Полается с обжаренными овощами в дополнение идёт Пико-де-гайо, соус из авакадо и греческий йогурт.',
                        me: 'Fajitas su jedno od najpoznatijih meksičkih jela, koje je postalo popularno daleko izvan Latinske Amerike. Služi se sa pečenim povrćem, uz piko de gajo, sos od avokada i grčki jogurt.'
                    },
                    checkAvailability: {
                        en: 'Sezonsko jelo! Pitajte za dostupnost!',
                        ru: 'Сезонное блюдо! Спрашивайте о наличии.',
                        me: 'Seasonal dish! Ask for availability.',
                    },
                    img: '/img/menu/main_course/fahitos.webp',
                    weight: 450,
                    price: 19
                },
            ]
        },
        desserts: {
            title: {
                en: 'Desserts',
                ru: 'Десерты',
                me: 'Dezerti'
            },
            items: [
                {
                    title: {
                        en: 'Crema Catalana',
                        ru: 'Крем Каталана',
                        me: 'Krema Katalana'
                    },
                    description: {
                        en: 'Catalan or Catalan cream, creme catalana, also known as cream of St. Joseph, is a Spanish dessert, a traditional dish of Catalan cuisine. Similar to French creme brulee, but prepared with milk rather than cream.',
                        ru: 'Каталонский или каталанский крем, крем каталана, также известный, как крем святого Иосифа — испанский десерт, традиционное блюдо каталонской кухни. Схож с французским крем-брюле, но готовится на молоке, а не на сливках.',
                        me: 'Katalonski ili katalonski krem, creme catalana, poznat i kao krem od Svetog Josifa, španski je desert, tradicionalno jelo katalonske kuhinje. Slično francuskom krem bruleeu, ali pripremljeno sa mlekom, a ne kremom.'
                    },
                    img: '/img/menu/desserts/crema_catalana.webp',
                    weight: 100,
                    price: 3
                },
                {
                    title: {
                        en: 'Panna Cotta',
                        ru: 'Панакота',
                        me: 'Panna Cotta'
                    },
                    description: {
                        en: 'A Northern Italian dessert that we make with coconut milk and top it with raspberry jam.',
                        ru: 'Североитальянский десерт, который мы делаем на кокосовом молоке и сверкну поливаем малиновым джемом.',
                        me: 'Severnoitalijanski desert koji pravimo sa kokosovim mlekom i prelijemo ga džemom od malina.'
                    },
                    img: '/img/menu/desserts/panna_cotta.webp',
                    weight: 70,
                    price: 3
                },
                {
                    title: {
                        en: 'Tiramisu',
                        ru: 'Тирамису',
                        me: 'Tiramisu'
                    },
                    description: {
                        en: 'Tiramisu is an Italian multi-layer dessert containing mascarpone cheese, coffee, chicken eggs, sugar and savoiardi biscuits.',
                        ru: 'Тирамису́ — итальянский многослойный десерт, в состав которого входят сыр маскарпоне, кофе, куриные яйца, сахар и печенье савоярди.',
                        me: 'Tiramisu je italijanski višeslojni desert koji sadrži mascarpone sir, kafu, pileća jaja, šećer i savojardi keks.'
                    },
                    img: '/img/menu/desserts/tiramisu.webp',
                    weight: 200,
                    price: 5
                },
            ]
        },
        kids_menu: {
            title: {
                en: 'Kids menu',
                ru: 'Детское меню',
                me: 'Dečiji meni'
            },
            items: [
                {
                    title: {
                        en: 'Chicken noodle soup',
                        ru: 'Суп куриный с лапшой',
                        me: 'Pileća supa sa rezancima'
                    },
                    description: {
                      ru: 'Классический чистый куриный бульон с морковью, цукини и куриным кнелями. Прекрасный и самое гипоаллергенный суп для вашего ребенка.',
                      en: 'Classic clear chicken broth with carrots, zucchini and chicken quenelles. A wonderful and most hypoallergenic soup for your child.',
                      me: 'Klasična bistra pileći bujon sa šargarepom, tikvicama i pilećim kenelima. Divna i najhipoalergenija supa za vaše dete.'
                    },
                    img: '/img/menu/kids_menu/chicken_noodle_soup.webp',
                    weight: 300,
                    price: 5
                },
                {
                    title: {
                        en: 'Linguini with meatballs and tomato sauce',
                        ru: 'Лингвини с томатом и фрикадельками',
                        me: 'Linguini sa paradajzom i ćufte'
                    },
                    description: {
                      ru: 'Классическая паста с томатным соусом и кнелями из говядины с зеленью. Подаётся с натертым сыром Пармезан.',
                      en: 'Classic pasta with tomato sauce and beef quenelles with herbs. Served with grated Parmesan cheese.',
                      me: 'Klasična pasta sa paradajz sosom i goveđim kenelima sa začinskim biljem. Služi se sa rendanim parmezanom.',
                    },
                    img: '/img/menu/kids_menu/linguini_with_meatballs_and_tomato_sauce.webp',
                    weight: 260,
                    price: 5
                },
                {
                    title: {
                        en: 'Chicken strips with fries',
                        ru: 'Куриные стрипсы с картошкой фри',
                        me: 'Hrskava piletina sa pomfritom'
                    },
                    description: {
                      en: 'Chicken breast in Asian sauce, based on Worcestershire and soy sauce, lime juice, and its zest, is served fried with French fries and two sauces: sweet chili and ketchup.',
                      ru: 'Куриная грудка в в азиатском соусе на основе  вустерского и соевого соуса, соком лайма и его цедрой. Подается обжаренная в панировке картофелем фри и двумя соусами, сладкий Чили и кетчуп.',
                      me: 'Kokošija prsa u azijskom sosu, na bazi vusteršir i soja sosa, soka od limuna i njegove korice. Poslužuje se pržena sa pomfritom i dva sosa: slatki čili i kečap.'
                    },
                    img: '/img/menu/kids_menu/chicken_strips.webp',
                    weight: 250,
                    price: 5.5
                },
                {
                    title: {
                        en: 'Сarbonara',
                        ru: 'Карбонара',
                        me: 'Karbonara'
                    },
                    description: {
                      ru: 'Cпагетти с мелкими кусочками панчетты, смешанная с соусом из яиц, сыра пекорино романо и грано подано, и свежемолотого чёрного перца. Не избитая классика Итальянской кухни.',
                      en: `Spaghetti Cacio e Pepe is an authentic Italian dish that combines spaghetti with finely diced pancetta, a sauce made from eggs, Pecorino Romano cheese, and freshly ground black pepper. It's a classic that's not to be missed in Italian cuisine.`,
                      me: 'Špagete Kačo e Pepe je autentično italijansko jelo koje kombinuje špagete sa fino naseckanim pančetom, sosom od jaja, sira Pekorino Romano i sveže mlevenim crnim biberom. Ovo je klasično jelo koje ne smete propustiti u italijanskoj kuhinji.'
                    },
                    img: '/img/menu/pasta_and_risotto/carbonara.webp',
                    weight: 250,
                    price: 9.5
                },
            ]
        }
    })

    return { menu, useMenuStore, booking, language, sloganFirst, sloganSecond, categories }
})

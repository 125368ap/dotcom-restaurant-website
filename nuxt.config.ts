// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxt/image',
    'nuxt-swiper',
    '@vueuse/nuxt',
    '@pinia/nuxt'
  ],
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],
  components: {
    dirs: [
      '~/components/layouts',
      '~/components/app',
      '~/components/social',
      '~/components/ui',
      {
        'path': '~/components/global',
        'global': true
      },
    ]
  },
})

/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    extend: {
      colors: {
        primary: '#FF7A00'
      }
    },
  },
  plugins: [],
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {}
      }
    }
  },
  tailwindcss: {
    cssPath: '~/assets/styles/main.css'
  },
}

